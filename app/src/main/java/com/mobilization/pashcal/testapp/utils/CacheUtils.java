package com.mobilization.pashcal.testapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.Period;

import java.lang.reflect.Type;

/**
 * Utils for work with cache
 * @param <T> type of cache data
 */
public class CacheUtils<T> {

    private static final String CACHE_KEY = "local_cache";
    private final String CACHE_TIME_KEY;
    private final String CACHE_DATA_KEY;

    private final Period _relevancePeriod;
    private final Context _context;
    private Type _dataTypeForJson;

    public CacheUtils(Context context, String key, Period relevancePeriod) {
        _context = context;
        CACHE_TIME_KEY = key + "_time";
        CACHE_DATA_KEY = key + "_data";
        _relevancePeriod = relevancePeriod;
    }

    public CacheUtils(Context context, String key, Period relevancePeriod, Type typeForJson) {
        this(context, key, relevancePeriod);
        _dataTypeForJson = typeForJson;
    }

    /**
     * Check relevance of cache
     */
    public boolean isActual() {
        SharedPreferences cache = _context.getSharedPreferences(CACHE_KEY, Context.MODE_PRIVATE);
        if (cache == null || !cache.contains(CACHE_TIME_KEY))
            return false;

        DateTime lastUpdateTime = new DateTime().withMillis(cache.getLong(CACHE_TIME_KEY, 0));
        if (lastUpdateTime.plus(_relevancePeriod).compareTo(Instant.now()) < 0)
            return false;

        return true;
    }

    public boolean isEmpty() {
        SharedPreferences cache = _context.getSharedPreferences(CACHE_KEY, Context.MODE_PRIVATE);
        if (cache == null || !cache.contains(CACHE_DATA_KEY))
            return true;

        return false;
    }

    /**
     * Get last data from cache
     */
    public T getCacheData() {
        SharedPreferences cache = _context.getSharedPreferences(CACHE_KEY, Context.MODE_PRIVATE);
        if (cache == null || !cache.contains(CACHE_DATA_KEY))
            return null;

        String dataJson = cache.getString(CACHE_DATA_KEY, "");
        try {
            Type typeJson;
            if (_dataTypeForJson != null)
                typeJson = _dataTypeForJson;
            else
                typeJson = new TypeToken<T>(){}.getType();
            T data = (T)new Gson().fromJson(dataJson, typeJson);
            return data;
        }
        catch (Exception e) {
            Log.e("GET_CACHE_DATA", e.toString());
            return null;
        }
    }

    /**
     * Update data in local cache
     */
    public void updateCacheData(T newData) {
        String dataJson;
        try {
            dataJson = new Gson().toJson(newData);
        }
        catch (Exception e) {
            Log.e("GET_CACHE_DATA", e.toString());
            return;
        }

        SharedPreferences cache = _context.getSharedPreferences(CACHE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor cacheEditor = cache.edit();
        cacheEditor.putLong(CACHE_TIME_KEY, Instant.now().getMillis());
        cacheEditor.putString(CACHE_DATA_KEY, dataJson);
        cacheEditor.apply();
    }
}
