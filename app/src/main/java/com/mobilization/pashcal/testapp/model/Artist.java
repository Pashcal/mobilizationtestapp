package com.mobilization.pashcal.testapp.model;

/**
 * Yandex mobilization Artist model
 */
public class Artist {

    public int id;
    public String name;
    public String[] genres;
    public int tracks;
    public int albums;
    public String link;
    public String description;
    public Image cover;

    public class Image {
        public String small;
        public String big;
    }
}