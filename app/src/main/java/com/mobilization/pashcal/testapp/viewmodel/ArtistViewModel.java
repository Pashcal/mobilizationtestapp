package com.mobilization.pashcal.testapp.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.TextUtils;

import com.mobilization.pashcal.testapp.model.Artist;
import com.mobilization.pashcal.testapp.utils.StringUtils;

/**
 * ViewModel for artist view
 */
public class ArtistViewModel extends BaseObservable {

    private final Context _context;
    private Artist _artist;

    public ArtistViewModel(Context context) {
        _context = context;
    }

    public void setArtist(Artist artist) {
        _artist = artist;
    }

    @Bindable
    public String getName() {
        return _artist.name;
    }
    @Bindable
    public String getCover() {
        return _artist.cover.big;
    }
    @Bindable
    public String getGenres() {
        return TextUtils.join(", ", _artist.genres);
    }
    @Bindable
    public String getSongs() {
        return String.format("%d %s   ·   %d %s",
                _artist.albums, StringUtils.rusAfterNum(_artist.albums, "альбом", "альбома", "альбомов"),
                _artist.tracks, StringUtils.rusAfterNum(_artist.tracks, "песня", "песни", "песен"));
    }
    @Bindable
    public String getDescription() {
        return _artist.description;
    }
}
