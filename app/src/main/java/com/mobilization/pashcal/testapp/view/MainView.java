package com.mobilization.pashcal.testapp.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.mobilization.pashcal.testapp.R;
import com.mobilization.pashcal.testapp.databinding.MainViewBinding;
import com.mobilization.pashcal.testapp.viewmodel.MainViewModel;

public class MainView extends AppCompatActivity {

    private final MainViewModel _viewModel = new MainViewModel(this);
    private MainViewBinding _binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _binding = DataBindingUtil.setContentView(MainView.this, R.layout.main_view);
        _binding.artistsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        _binding.setMain(_viewModel);
    }

    @Override
    protected void onStart() {
        super.onStart();
        _viewModel.loadArtists();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
