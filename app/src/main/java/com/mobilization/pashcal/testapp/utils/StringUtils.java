package com.mobilization.pashcal.testapp.utils;

public class StringUtils {

    /**
     * Return correct russian word after number
     */
    public static String rusAfterNum(int quantity, String one, String two, String five) {
        int q = (quantity % 100 > 20) ? quantity % 10 : quantity % 20;
        switch (q)
        {
            case 1: return one;
            case 2: case 3: case 4: return two;
            default: return five;
        }
    }
}
