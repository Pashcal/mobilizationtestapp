package com.mobilization.pashcal.testapp.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.TextUtils;

import com.mobilization.pashcal.testapp.model.Artist;
import com.mobilization.pashcal.testapp.utils.StringUtils;

/**
 * ViewModel for main recyclerView row
 */
public class ArtistRowVM extends BaseObservable {

    private Artist _artist;

    public ArtistRowVM(Artist artist) {
        _artist = artist;
    }

    public Artist getArtist() {
        return _artist;
    }

    @Bindable
    public String getCover() {
        return _artist.cover.small;
    }
    @Bindable
    public String getName() {
        return _artist.name;
    }
    @Bindable
    public String getGenres() {
        return TextUtils.join(", ", _artist.genres);
    }
    @Bindable
    public String getSongs() {
        return String.format("%d %s, %d %s",
                _artist.albums, StringUtils.rusAfterNum(_artist.albums, "альбом", "альбома", "альбомов"),
                _artist.tracks, StringUtils.rusAfterNum(_artist.tracks, "песня", "песни", "песен"));
    }
}
