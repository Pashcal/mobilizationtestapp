package com.mobilization.pashcal.testapp.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.mobilization.pashcal.testapp.R;

public class AlertUtils {

    /**
     * Show simple alert with OK button
     */
    public static void showAlert(Context context, String message, String title) {
        if (context == null || message.isEmpty()) {
            Log.e("ERROR_ALERT", "Null context or empty message.");
            return;
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setMessage(message)
                .setTitle(title)
                .setPositiveButton(R.string.okBtn_title, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                });
        dialogBuilder.show();
    }

    /**
     * Show simple alert with OK button
     */
    public static void showAlert(Context context, int messageId, Integer titleId) {
        if (context == null) {
            Log.e("ERROR_ALERT", "Null context.");
            return;
        }

        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            dialogBuilder.setMessage(messageId)
                    .setPositiveButton(R.string.okBtn_title, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {}
                    });
            if (titleId != null)
                dialogBuilder.setTitle(titleId);
            dialogBuilder.show();
        }
        catch (Resources.NotFoundException ex) {
            Log.e("ERROR_ALERT", ex.toString());
        }
    }
}
