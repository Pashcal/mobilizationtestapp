package com.mobilization.pashcal.testapp.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobilization.pashcal.testapp.BR;
import com.mobilization.pashcal.testapp.R;
import com.mobilization.pashcal.testapp.model.Artist;
import com.mobilization.pashcal.testapp.network.YandexMobAPI;
import com.mobilization.pashcal.testapp.utils.AlertUtils;
import com.mobilization.pashcal.testapp.utils.CacheUtils;
import com.mobilization.pashcal.testapp.view.ArtistView;

import net.droidlabs.mvvm.recyclerview.adapter.ClickHandler;
import net.droidlabs.mvvm.recyclerview.adapter.binder.ItemBinder;
import net.droidlabs.mvvm.recyclerview.adapter.binder.ItemBinderBase;

import org.joda.time.Period;

import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ViewModel for main view
 */
public class MainViewModel extends BaseObservable {

    private final String ARTISTS_CACHE_KEY = "artists_cache";
    private final Context _context;
    private final ObservableArrayList<ArtistRowVM> _artists;
    private final ItemBinderBase<ArtistRowVM> _artistViewBinder;
    private final ClickHandler<ArtistRowVM> _artistClickHandler;
    private final CacheUtils<List<Artist>> _artistsCache;

    private boolean _isLoading;

    public MainViewModel(Context context) {
        _context = context;
        _artists = new ObservableArrayList<>();
        _artistViewBinder = new ItemBinderBase<>(BR.artistRow, R.layout.artist_row);
        _artistClickHandler = new ClickHandler<ArtistRowVM>() {
            @Override
            public void onClick(ArtistRowVM viewModel) {
                //serialize artist and open artist view
                if (viewModel != null) {
                    String artistJson = new Gson().toJson(viewModel.getArtist());
                    _context.startActivity(new Intent(_context, ArtistView.class)
                            .putExtra(ArtistView.ARTIST_BUNDLE_KEY, artistJson));
                }
            }
        };
        _artistsCache = new CacheUtils<List<Artist>>(_context, "artist",
                Period.minutes(10), new TypeToken<List<Artist>>(){}.getType());
    }

    @Bindable
    public ObservableArrayList<ArtistRowVM> getArtists() {
        return _artists;
    }
    private void setArtists(List<? extends Artist> artists) {
        if (artists != null) {
            for (int i = artists.size() - 1; i >= 0; i--)
                _artists.add(0, new ArtistRowVM(artists.get(i)));
        }
    }

    @Bindable
    public ItemBinder<ArtistRowVM> getArtistViewBinder()
    {
        return _artistViewBinder;
    }

    @Bindable
    public ClickHandler<ArtistRowVM> getArtistClickHandler() {
        return _artistClickHandler;
    }

    @Bindable
    public boolean isLoading() {
        return _isLoading;
    }
    private void setLoading(boolean isLoading) {
        _isLoading = isLoading;
        notifyPropertyChanged(BR.loading);
    }


    /**
     * Load all artists from cache or from Yandex
     */
    public void loadArtists() {

        //check cache

        setLoading(true);
        _artists.clear();
        if (!_artistsCache.isActual()) {
            //load from yandex
            ConnectivityManager connMgr = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new LoadArtistsTask().execute();
                return;
            } else if (_artistsCache.isEmpty()) {
                AlertUtils.showAlert(_context, R.string.noConnection_message, R.string.errorAlert_title);
            }
        }

        //else load cache
        if (!_artistsCache.isEmpty())
            setArtists(_artistsCache.getCacheData());
        setLoading(false);
    }

    /**
     * AsyncTask to load artists from Yandex
     */
    private class LoadArtistsTask extends AsyncTask<Void, Void, List<Artist>> {

        @Override
        protected List<Artist> doInBackground(Void... voids) {

            //start loading animation

            //load artist data
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://download.cdn.yandex.net/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            YandexMobAPI yandexMob = retrofit.create(YandexMobAPI.class);
            try {
                Response<List<Artist>> artistsResponse = yandexMob.artists().execute();
                //emulate slow connection
                Thread.sleep(3000);
                return artistsResponse.body();
            }
            catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Artist> loadedArtists) {

            //end loading animation

            //check loaded data
            if (loadedArtists != null && !loadedArtists.isEmpty()) {
                //update view model
                setArtists(loadedArtists);
                //update cache
                _artistsCache.updateCacheData(loadedArtists);
                setLoading(false);
                return;
            }

            if (loadedArtists == null) {
                AlertUtils.showAlert(_context, R.string.unknownError_message, R.string.errorAlert_title);
            }
            else if (loadedArtists.isEmpty()) {
                AlertUtils.showAlert(_context, R.string.emptyLoadedData_message, null);
            }

            //else load cache
            if (!_artistsCache.isEmpty())
                setArtists(_artistsCache.getCacheData());
            setLoading(false);
        }
    }
}
