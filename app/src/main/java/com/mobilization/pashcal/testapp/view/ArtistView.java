package com.mobilization.pashcal.testapp.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobilization.pashcal.testapp.R;
import com.mobilization.pashcal.testapp.databinding.ArtistViewBinding;
import com.mobilization.pashcal.testapp.model.Artist;
import com.mobilization.pashcal.testapp.utils.AlertUtils;
import com.mobilization.pashcal.testapp.viewmodel.ArtistViewModel;

public class ArtistView extends AppCompatActivity {

    public static final String ARTIST_BUNDLE_KEY = "selected_artist";
    private final ArtistViewModel _viewModel = new ArtistViewModel(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.artist_view);

        //get bundle with artist data
        Bundle artistBundle;
        if (savedInstanceState != null)
            artistBundle = savedInstanceState;
        else
            artistBundle = getIntent().getExtras();

        //deserialize artist and update view model
        Artist artist = null;
        try {
            String artistJson = artistBundle.getString(ARTIST_BUNDLE_KEY);
            artist = new Gson().fromJson(artistJson, new TypeToken<Artist>(){}.getType());
        }
        catch (Exception e) {
            //show error alert
            AlertUtils.showAlert(this, R.string.unknownError_message, R.string.errorAlert_title);
            artist = new Artist();
        }
        finally {
            setTitle(artist != null ? artist.name : "");
            _viewModel.setArtist(artist);
        }

        ArtistViewBinding binding = DataBindingUtil.setContentView(ArtistView.this, R.layout.artist_view);
        binding.setArtist(_viewModel);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
