package com.mobilization.pashcal.testapp.network;

import com.mobilization.pashcal.testapp.model.Artist;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Yandex API for mobilization school
 */
public interface YandexMobAPI {

    /**
     * GET request for artists
     */
    @GET("mobilization-2016/artists.json")
    Call<List<Artist>> artists();
}
